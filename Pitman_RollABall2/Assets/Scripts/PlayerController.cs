﻿using UnityEngine;

// Include the namespace required to use Unity UI and Input System
using UnityEngine.InputSystem;
using TMPro;

public class PlayerController: MonoBehaviour
{

	// Create public variables for player speed, and for the Text UI game objects
	public float speed;
	public TextMeshProUGUI countText;
	public GameObject winTextObject;

	public float pushForce;

	private float movementX;
	private float movementY;

	private Rigidbody rb;
	private int count;

	// At the start of the game..
	void Start()
	{
		// Assign the Rigidbody component to our private rb variable
		rb = GetComponent<Rigidbody>();

		// Set the count to zero 
		count = 0;

		SetCountText();

		// Set the text property of the Win Text UI to an empty string, making the 'You Win' (game over message) blank
		winTextObject.SetActive(false);
	}

	void FixedUpdate()
	{
		// Create a Vector3 variable, and assign X and Z to feature the horizontal and vertical float variables above
		Vector3 movement = new Vector3(movementX, 0.0f, movementY);

		rb.AddForce(movement * speed);
	}

	void OnTriggerEnter(Collider other)
	{
		// ..and if the GameObject you intersect has the tag 'PickUp' assigned to it..
		if (other.gameObject.CompareTag("PickUp"))
		{
			other.gameObject.SetActive(false);

			// Add one to the score variable 'count'
			count = count + 1;

			// Run the 'SetCountText()' function (see below)
			SetCountText();
		}
	}
	
	void OnCollisionEnter(Collision other)
	{
		if (other.gameObject.tag == "WallTag")
		{
			Debug.Log("hitWall");
			Vector3 Dir = other.relativeVelocity;
			Dir = Dir.normalized;
			//Dir = new Vector3(Dir.z, Dir.y, Dir.x);
			Debug.Log(Dir);
			rb.AddForce(Dir * pushForce, ForceMode.Impulse);
		}

	}

	void OnMove(InputValue value)
	{
		Vector2 v = value.Get<Vector2>();

		movementX = v.x;
		movementY = v.y;
	}

	void SetCountText()
	{
		countText.text = "Count: " + count.ToString();

		if (count >= 12)
		{
			// Set the text value of your 'winText'
			winTextObject.SetActive(true);
		}
	}

	float boostTimeLeft = 0f;
	float originalSpeed;
	public float boostSpeed;
	
	void Start ()
    {
		rb = GetComponent<Rigidbody>();
		count = 0;
		SetCountText ();
		winText.text = "";

		originalSpeed = speed;
    }

	private void Update()
	{
		if (boostTimeLeft > 0f)
		{
			boostTimeLeft -= Time.deltaTime;
			if (boostTimeLeft <= 0f)
			{
				speed = originalSpeed;
			}
		}
	}
	public void OnMove(InputValue movementValue)

    private void FixedUpdate()

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.CompareTag ("Pick Up"))
        {
			other.gameObject.SetActive(false);
			count = count + 1;
			SetCountText();

			speed = boostSpeed;
			boostTimeLeft = 3f;
        }
    }
}